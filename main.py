from telegram.ext import Updater, CommandHandler
import config
from yandex_translate import YandexTranslate


def main():
    updater = Updater(config.token)
    dp = updater.dispatcher

    def start(_, update):
        update.message.reply_text(config.messages["start"])

    def help(_, update):
        update.message.reply_text(config.messages["help"])

    def ruen(_, update, args):
        text = ' '.join(args)
        translate = YandexTranslate(config.yandex["token"])
        translation = translate.translate(text, 'ru-en')
        update.message.reply_text(translation["text"][0])

    def enru(_, update, args):
        text = ' '.join(args)
        translate = YandexTranslate(config.yandex["token"])
        translation = translate.translate(text, 'en-ru')
        update.message.reply_text(translation["text"][0])

    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help))
    dp.add_handler(CommandHandler("ruen", ruen, pass_args=True))
    dp.add_handler(CommandHandler("enru", enru, pass_args=True))

    updater.start_polling()


if __name__ == '__main__':
    main()
