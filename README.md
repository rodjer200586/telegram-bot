# @UserElixirBot
## Telegram Bot

https://t.me/UserElixirBot

### Доступные команды

* /start - Начинаем работу с ботом
* /help - Список доступных команд
* /ruen text - Перевод текста с русского на английский
* /enru text - Перевод текста с английского на русский